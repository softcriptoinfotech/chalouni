/**
 * Print coloured out put in terminal
 *
 * @packageDescription
 */

export function warn(txt: string) {
  console.warn("\x1b[33m", txt);
}

export function error(txt: string) {
  console.warn("\x1b[41m", txt);
}

export function success(txt: string) {
  console.warn("\x1b[32m", txt);
}
