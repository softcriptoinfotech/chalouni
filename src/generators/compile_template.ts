import fs from "fs";
import Handlebars from "handlebars";
import util from "util";
const readFile = util.promisify(fs.readFile);

export function compileTemplate(filePath: string, data: unknown) {
  // async in Promise callback doesn't like by eslint
  async function _callme(resolve: any, reject: any) {
    try {
      const source = await readFile(filePath); //read template
      resolve(Handlebars.compile(source.toString())(data));
    } catch (error) {
      reject(error);
    }
  }
  // return Promise
  return new Promise((resolve, reject) => {
    _callme(resolve, reject);
  });
}
