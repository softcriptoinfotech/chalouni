import fs from "fs";
import util from "util";
import path from "path";
import * as print from "../print";

import { compileTemplate } from "./compile_template";

const writeFile = util.promisify(fs.writeFile);

interface IFileGeneratorOption {
  /**
   * The name of entity we are creating
   * User, Post or any valid word
   */
  entity: string;
  dir: string;
  templateUrl: string;
}

type entityType = "models" | "controllers" | "interface";

/**
 *
 * @param name - name of entity
 * @param destDir
 */
export async function generate(
  category: entityType,
  options: IFileGeneratorOption
) {
  try {
    // if interface, file name becomes IUser if options.entity = User
    const filename =
      category === "interface" ? `I${options.entity}` : options.entity;

    // root + category + file name + .ts == /path/to/project/src/controllers/User.ts
    // interface = /path/to/project/src/interface/IUser.ts
    // model = /path/to/project/src/models/User.ts
    const destination = path.resolve(options.dir, category, filename + ".ts");

    if (fs.existsSync(destination)) {
      return print.error(`Already exists ${destination}.`);
    }
    const source = await compileTemplate(options.templateUrl, {
      entity: options.entity,
    });
    await writeFile(destination, source as string);
    print.success(`Created  ${destination}`);
  } catch (error) {
    print.error(error);
  }
}
