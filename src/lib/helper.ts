/**
 * Capitalises First Letter
 * @param name
 */
export function toUpperCaseFirstLetter(name: string) {
  let firstCharactorCapitalised = name.charAt(0).toUpperCase();
  firstCharactorCapitalised += name.slice(1);
  return firstCharactorCapitalised;
}
