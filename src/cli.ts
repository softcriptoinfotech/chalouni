#! /usr/bin/env node

import path from "path";
import * as yargs from "yargs";
import { generate } from "./generators";
import { toUpperCaseFirstLetter } from "./lib/helper";

const templates = {
  models: path.resolve(__dirname, "..", "templates", "model.txt"),
  interface: path.resolve(__dirname, "..", "templates", "interface.model.txt"),
  controllers: path.resolve(__dirname, "..", "templates", "controller.txt"),
};

// Location the script ran from
const destination = process.cwd() + "/src";

/*
 * For more details [visit](https://github.com/yargs/yargs/blob/master/docs/typescript.md)
 * http://yargs.js.org/docs/
 */
const argv = yargs
  .option("model", {
    alias: "m",
    describe: "create model",
  })
  .option("controller", {
    alias: "c",
    describe: "create controller",
  })
  .option("snippet", {
    type: "boolean",
    alias: "s",
    describe: "creates snippets into console",
  })
  .option("interface", {
    alias: "i",
    type: "string",
    describe: "output interface snippet to console.",
  })
  .option("db", {
    alias: "d",
    describe: "output database schema for mongoose document.",
    type: "string",
  })
  .help().argv;

// Create model and interface
if (argv.model) {
  const entity = toUpperCaseFirstLetter(argv.model as string);
  generate("models", {
    entity,
    dir: destination,
    templateUrl: templates.models,
  });

  generate("interface", {
    entity,
    dir: destination,
    templateUrl: templates.interface,
  });
}

// create controller
if (argv.controller) {
  const entity = toUpperCaseFirstLetter(argv.controller as string);
  generate("controllers", {
    entity,
    dir: destination,
    templateUrl: templates.controllers,
  });
}

// Output snippet in console
if (argv.snippet) {
  if (argv.db) {
    const list = argv.db.split(",");
    if (list.length) {
      console.log("{");
      list.forEach((item) => {
        console.log(`${item}: {type: String},`);
      });
      console.log("}");
    } else {
      console.log("Please pass the value");
    }
  }

  // create interface
  if (argv.interface) {
    const list = argv.interface.split(",");
    if (list.length) {
      const body: Array<string> = ["export interface IChangeMe {"];
      list.forEach((item) => {
        body.push(`${item}: string;`);
      });
      body.push("}");
      console.log(body.join("\n\t"));
    }
  }
}
