# chalouni

CLI tool to create

- code snippets and
- files with code snippets.

**Files:**

- Controller
- Model
- Interface

**Snippets**

- Interface
- Mongoose document schema

[TOC]

# Install

```sh
npm install chalouni -g
```

Install this tool in global scope.

**If installed local scope**
run it using `npx chalouni`

# Usage

CD to your project `cd path/to/your-project`

## Controller File

creates controller

```sh
chalouni -c user
```

Creates new file `src/controllers/User.ts`

## Model and Interface File

creates both **model and interface**

```sh
chalouni -m user
```

Creates new files `src/models/User.ts` and `src/interface/IUser.ts`

# Snippets

Output code snippets to console.

Requires 2 parameters

1st option is `--snippet` or `-s`

other supported options are

- `-i` or `--interface` for interface
- `-d` or `--db` for mongodb document (mongoose schema)

> `i` and `d` options are only valid with `--snippet` or `-s` option

Example

```sh
# Interface snippet
chalouni --snippet -interface "name,age,birthday"

# Alias for above code
chalouni -s -i "name,age,birthday"

# Mongoose document schema snippet
chalouni --snippet -db "name, age, birthday"

# Alias for above code
chalouni -s -d "name,age,birthday"
```

**Output example**:

```javascript
// Interface snippet
export interface IChangeMe {
  name: string;
  age: string;
  birthday: string;
}
// Mongoose schema snippet
{
  name: {type: String},
  age: {type: String},
  birthday: {type: String},
}
```

# Structure

- **./template** - handlebar template
